import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./app/App";
import EditUserPage from "./app/EditUserPage";
import NewUserPage from "./app/NewUserPage";
import { Provider } from "react-redux";
import store from "./redux/store";
import {
  createBrowserRouter,
  RouterProvider,
  Route,
} from "react-router-dom";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);


const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
  {
    path: "/user/edit/:id",
    element: <EditUserPage />,
  },
  {
    path: "/user/new",
    element: <NewUserPage />,
  },
]);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);

