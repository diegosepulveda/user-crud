import { UserRepositoryInterface } from "../repository/UserRepositoryInterface";
import { IUser } from '../../app/types/UserEntity'


export class UserRegister implements UserRepositoryInterface {

    private client : any;
    constructor(httpClient :any) {
        this.client = httpClient;
    }
    
    async getUsers(): Promise<IUser[]> {
        const response = await this.client.get('users');
        return response.data
    }
    async getUser(id:string): Promise<IUser> {
        const response = await this.client.get(`users/${id}`);
        return response.data
    }
    async createUser(user:IUser): Promise<IUser> {
        const response = await this.client.post(`users`,user);
        return response.data
    }
    async updateUser(id:string, user:IUser): Promise<IUser> {
        const response = await this.client.put(`users/${id}`,user);
        return response.data
    }
    async deleteUser(id:string): Promise<boolean> {
        const response = await this.client.delete(`users/${id}`);
        return response.status === 200 ? true : false
    }
}



