import { useEffect, useState } from 'react';
import {  useParams } from 'react-router-dom';
import UserNewEditForm from './components/UserNewEditForm';
import axios from '../app/utils/axios'
import { UserRegister } from "../app/data/UserRegister";
import { IUser } from '../app/types/UserEntity'

const userRegister = new UserRegister(axios);

function EditUserPage() {
    let { id } = useParams();
    const [currentUser, setCurrentUser] = useState<IUser | null>(null)
    
    useEffect(()=> {
        const fetchUser = async() =>{
            if(id !== undefined) {
                const user = await userRegister.getUser(id);
                setCurrentUser(user);
            }
        }
        fetchUser()
    },[])
    
    return (<div>
        <h1>Editar Usuario</h1>
        {
            currentUser && <UserNewEditForm isEdit currentUser={currentUser} />
        }
    </div>)
  }

export default EditUserPage;