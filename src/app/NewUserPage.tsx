import { useEffect } from 'react';
import {  useParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from "../redux/hooks";
import UserNewEditForm from './components/UserNewEditForm';

function NewUserPage() {
    let { id } = useParams();
    const user  = useAppSelector(state => state.users.users.find(user => user.id === id));
    
    // useEffect(()=> {
    //     console.log(users);
    // },[users])

    return (<div>
      <h1>Crear Usuario</h1>
       <UserNewEditForm />
    </div>)
  }

export default NewUserPage;