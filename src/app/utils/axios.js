import axios from 'axios';
import { HOST_API } from '../config';
const axiosInstance = axios.create({ baseURL: process.env.REACT_APP_API_BASE_URL });
export default axiosInstance;
