import { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from "../redux/hooks";
import { getUsers } from "../redux/slices/users";
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from "@mui/icons-material/Edit";
import { useNavigate } from 'react-router-dom';
import {deleteUser} from '../redux/slices/users'
import Button from '@mui/material/Button';
import { Box } from '@mui/material';
import Avatar from '@mui/material/Avatar';

function Home() {
    const {users} = useAppSelector(state => state.users);
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    
    useEffect(() => {
        if(users.length === 0) {
            dispatch(getUsers())
        }
    },[]);
    
    const handleCreate = () => {
        navigate(`user/new`)
    }
    const handleEditUser = (id :string) => {
        navigate(`user/edit/${id}`)
    }

    const handleDeleteUser = (id :string) => {
        dispatch(deleteUser(id));
    }

    return (
        <Box>
            <Button  onClick={handleCreate} variant="contained" size="medium">Crear</Button>
            <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
                <TableRow>
                <TableCell align="right">Nombre</TableCell>
                <TableCell align="right">Apellido</TableCell>
                <TableCell align="right">Correo</TableCell>
                <TableCell align="right">Avatar</TableCell>
                <TableCell align="right">Acciones</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {users.map((row) => (
                <TableRow
                    key={row.id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                    <TableCell align="right">{row.first_name}</TableCell>
                    <TableCell align="right">{row.second_name}</TableCell>
                    <TableCell align="right">{row.email}</TableCell>
                    <TableCell align="right">
                        <Avatar alt={row.first_name} src={row.avatar} />
                    </TableCell>
                    <TableCell align="right">
                        <IconButton onClick={() => handleEditUser(row.id)} aria-label="edit" color="primary">
                            <EditIcon/>
                        </IconButton>
                        <IconButton onClick={() => handleDeleteUser(row.id)} aria-label="delete" color="primary">
                            <DeleteIcon/>
                        </IconButton>
                    </TableCell>
                </TableRow>
                ))}
            </TableBody>
            </Table>
        </TableContainer>
        </Box>
    );
  }
  
  export default Home;