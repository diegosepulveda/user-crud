import {IUser} from '../types/UserEntity'

export interface UserRepositoryInterface {
    getUsers(): Promise<IUser[]>;
    getUser(id:string): Promise<IUser>|null;
    createUser(user:IUser): Promise<IUser>|null;
    updateUser(id:string, item:IUser): Promise<IUser>|null;
    deleteUser(id:string): Promise<boolean>|null;

  }