import { useAppDispatch, useAppSelector } from "../redux/hooks";
import { changeMessage, selectMessage } from "../redux/slices/basicSlice";

import "./App.css";
import Home from "./Home";

function App() {
  const message = useAppSelector(selectMessage);
  const dispatch = useAppDispatch();

  return (
    <Home />
  );
}

export default App;
