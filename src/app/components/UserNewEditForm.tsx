import { useEffect } from 'react';

import TextField from '@mui/material/TextField';
import {IUser} from '../types/UserEntity'
import { useForm, SubmitHandler } from "react-hook-form";
import { useNavigate } from 'react-router-dom';
import { LoadingButton } from '@mui/lab';
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import {addUser} from '../../redux/slices/users'
import Button from '@mui/material/Button';

type Props = {
    isEdit?: boolean;
    currentUser?: IUser | null;
  };

function UserNewEditForm({ isEdit = false, currentUser }: Props) {
    const dispatch = useAppDispatch();
    const { register,reset, handleSubmit,formState: { isSubmitting } } = useForm<IUser>({
        defaultValues : {
            first_name : currentUser?.first_name || '',
            second_name : currentUser?.second_name || '',
            email : currentUser?.email || '',
            avatar : currentUser?.avatar || '',
        }
    });
    const navigate = useNavigate();

    const createUserObject = (actualUser: IUser | {}, user: IUser) => {
        const newUser = {...actualUser,...user}
        return newUser;
    }

    const onSubmit = (data: IUser) => {
        const user = createUserObject(currentUser || {},data);
        dispatch(addUser(user))
        navigate("/");
    };

    const handleCancel = () => {
        navigate("/");
    }
    const handleDelete = () => {
        navigate("/");
    }

    return (
            <form onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <input {...register("first_name") } />
                </div>
                <div>
                    <input {...register("second_name") } />
                </div>
                <div>
                    <input {...register("email") } />
                </div>
                <div>
                    <input {...register("avatar") } />
                </div>
                <Button onClick={handleCancel} variant="outlined" >Atras</Button>
                <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
                    {!isEdit ? 'Crear' : 'Guardar'}
                </LoadingButton>
                <Button onClick={handleCancel} color='error' variant="outlined" >Cancelar</Button>
                {
                    isEdit && (<Button onClick={handleDelete} color='error' variant="contained" >Borrar</Button>)
                }
                
            </form>
    )
  }

export default UserNewEditForm;