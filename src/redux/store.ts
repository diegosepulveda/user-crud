import { combineReducers, configureStore } from "@reduxjs/toolkit";
import basicSlice from "./slices/basicSlice";
import users from "./slices/users";

const store = configureStore({
  reducer: combineReducers({
    basic: basicSlice,
    users,
  }),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store;
