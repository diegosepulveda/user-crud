import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";
import { Dispatch } from "@reduxjs/toolkit";
import axios from '../../app/utils/axios'
import { IUser } from '../../app/types/UserEntity'
import { UserRegister } from "../../app/data/UserRegister";


const listUser : IUser[] = [];

export const slice = createSlice({
  name: "users",
  initialState: {
    users: listUser,
    actualUser : null
  },
  reducers: {
    setActualUser: (state, action) => {
      state.actualUser = action.payload;
    },
    setUserList: (state, action) => {
      state.users = action.payload;
    },
    addUser: (state, action) => {
      state.users = [...state.users,action.payload];
    },
    editUser: (state, action) => {
      const updateUser = action.payload;
      state.users = state.users.map((o) => (o.id === updateUser.id ? updateUser : o));
    },
    removeUser: (state, action) => {
      const id = action.payload;
      const newListUsers = state.users.filter((user) => user.id !== id);
      state.users = newListUsers;

    },
  },
});


export function getUsers() {
  return async (dispatch : Dispatch) => {
    try {
      const userRegister = new UserRegister(axios);
      dispatch(slice.actions.setUserList(await userRegister.getUsers()));
    } catch (error) {
    }
  };
}

export function getUser(id:string) {
  return async (dispatch : Dispatch, getState: () => any) => {
    try {
      const userRegister = new UserRegister(axios);
      dispatch(slice.actions.setActualUser(await userRegister.getUser(id)));
    } catch (error) {
    }
  };
}
export function deleteUser(id:string) {
  return async (dispatch : Dispatch, getState: () => any) => {
    try {
      const userRegister = new UserRegister(axios);
      await userRegister.deleteUser(id);
      dispatch(slice.actions.removeUser(id));
    } catch (error) {
    }
  };
}

export function addUser(user : IUser) {
  return async (dispatch : Dispatch) => {
    try {
      const userRegister = new UserRegister(axios);
      if(user.id !== undefined) {
        await userRegister.updateUser(user.id,user);
        dispatch(slice.actions.editUser(user));
      } else {
        const newUser = await userRegister.createUser(user);        
        dispatch(slice.actions.addUser(newUser));
      }
    } catch (error) {
    }
  };
}

export default slice.reducer;
